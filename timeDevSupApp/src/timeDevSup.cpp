/**
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include <pv/pvDatabase.h>
#include <pv/channelProviderLocal.h>

#include	<dbStaticLib.h>
#include	<dbAccess.h>	    /* includes dbDefs.h, dbBase.h, dbAddr.h, dbFldTypes.h */
#include	<recSup.h>		    /* rset */
#include	<dbConvert.h> 	    /* dbPutConvertRoutine */
#include	<dbConvertFast.h>	/* dbFastPutConvertRoutine */
#include	<initHooks.h>
#include	<epicsThread.h>
#include	<errlog.h>
#include	<iocsh.h>
#include	<special.h>
#include	<macLib.h>
#include	<epicsString.h>
#include	<dbAccessDefs.h>
#include	<epicsStdio.h>
#include    <initHooks.h>

// The following must be the last include for code database uses
#include <epicsExport.h>
#define epicsExportSharedSymbols

#include "timeDevSup.h"

timeDevSupDriver::timeDevSupDriver(const char *portName)
    : asynPortDriver(portName,
                     1,
                     asynInt32Mask | asynEnumMask,
                     asynInt32Mask | asynEnumMask,
                     0,
                     1,
                     0,
                     0)
{
    // asynStatus status;
    // const char *functionName = "timeDevSupDriver";

    // EVR parameters
    createParam(EvrTimestampString,           asynParamOctet,     &P_EVRTimestamp);
    createParam(EvrPulseTag,                  asynParamInt32,     &P_EVRPulseTag);
    createParam(EvrTimeRef,                   asynParamInt32,     &P_EVRTimeRef);

}

/** Called when asyn clients call pasynInt32->write().
  * This function sends a signal to the simTask thread if the value of P_Run has changed.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus timeDevSupDriver::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *paramName;
    const char* functionName = "writeInt32";
    static const char *driverName="timeDevSupDriver";


    /* Set the parameter in the parameter library. */
    status = (asynStatus) setIntegerParam(function, value);

    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

    if (function == P_EVRPulseTag) {
        /* Set TimeRef with the same value */
        status = (asynStatus) setIntegerParam(P_EVRTimeRef, value);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, name=%s, value=%d",
                  driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, name=%s, value=%d\n",
              driverName, functionName, function, paramName, value);
    return status;
}

// called when asyn clients call pasynOctet->write()
asynStatus timeDevSupDriver::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual)
{
    int addr;
    int function = pasynUser->reason;
    const char *name;
    const char *paramName;
    asynStatus status = asynSuccess;
    int ret = 0;
    const char* functionName = "writeOctet";
    static const char *driverName="timeDevSupDriver";


    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    getParamName(function, &paramName);
    //asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %s", name, function, value);

    status = setStringParam(addr, function, (char *)value);

    if (function == P_EVRTimestamp) {
        // remember parsed timestamp values
        ret = sscanf(value, "%u.%u", &mTimeStampSec, &mTimeStampNsec);
        if (ret == 2) {
            // convert to EPICS time (epoch at 1990)
            mTimeStampSec -= POSIX_TIME_AT_EPICS_EPOCH;
            //asynPrintDriverInfo(pasynUser, "EPICS time stamp: %u sec %u nsec", mTimeStampSec, mTimeStampNsec);
            ret = 0;
        } else {
            //asynPrintError(pasynUser, "sscanf of timestamp returned %d", ret);
            ret = -1;
        }
    } 

    if (ret) {
        status = asynError;
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, name=%s, value=%s",
                  driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, name=%s, value=%s\n",
              driverName, functionName, function, paramName, value);
    return status;

    *nActual = nChars;
    return status;
}


/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int timeDevSupConfigure(const char *portName)
{
    new timeDevSupDriver(portName);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg * const initArgs[] = {&initArg0};
static const iocshFuncDef initFuncDef = {"timeDevSupConfigure",1,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    timeDevSupConfigure(args[0].sval);
}

void timeDevSupRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(timeDevSupRegister);

}

