#ifndef _TIMEDEVSUPDRIVER_H_
#define _TIMEDEVSUPDRIVER_H_

#include "asynPortDriver.h"

/* Asyn param strings */
#define EvrTimestampString                       "EVR.TIMESTAMP"
#define EvrPulseTag                              "EVR.PULSETAG"
#define EvrTimeRef                               "EVR.TIMEREF"

class timeDevSupDriver : public asynPortDriver {
public:
    timeDevSupDriver(const char *portName);

    /* These are the methods that we override from asynPortDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);

protected:
    int P_EVRTimestamp;
    int P_EVRPulseTag;
    int P_EVRTimeRef;

private:
    /* Our data */

public:
    // these two should be protected but need to be public since called from
    // myTimeStampSource()
    epicsUInt32 mTimeStampSec;
    epicsUInt32 mTimeStampNsec;
    epicsTimeStamp mInterruptTs;

};



#endif /*_TIMEDEVSUPDRIVER_H_*/